# -*- mode: ruby -*-
# vi: set ft=ruby :

# Specify minimum Vagrant version and Vagrant API version
Vagrant.require_version ">= 1.6.0"

require 'yaml'
require 'json'

gitlab_vagrant = YAML.load_file('gitlab-vagrant.yml')

Vagrant.configure("2") do |config|
  gitlab_vagrant.each do |gitlab_vagrant|
    config.vm.define gitlab_vagrant['name'] do |provision|

      # Common vars
      name = gitlab_vagrant['name']
      ip_address = gitlab_vagrant['ip_address']
      
      provision.vm.box = "ubuntu/xenial64"
      provision.vm.hostname = name
      provision.vm.network "private_network", ip: ip_address
      provision.vm.provider "virtualbox" do |vb|
        # Proxy DNS, useful if you're using a VPN that blocks access to other DNS providers
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        vb.memory = gitlab_vagrant['memory'] || "4096"
      end
      provision.vm.provider "vmware_fusion" do |v, override|
        override.vm.box = "bento/ubuntu-16.04"
      end

      # Build new hash that is sent to Ansible as extra_vars as JSON
      e_vars = Hash.new

      # Omnibus
      #e_vars['omnibus'] = "true" 
      e_vars['edition']          ||= gitlab_vagrant['edition']
      e_vars['version']          ||= gitlab_vagrant['version']
      e_vars['gitlab_runner']    ||= gitlab_vagrant['gitlab_runner']
      e_vars['elasticsearch']    ||= gitlab_vagrant['elasticsearch']
      e_vars['squid_proxy']      ||= gitlab_vagrant['squid_proxy']
      e_vars['elk']              ||= gitlab_vagrant['elk']
   
      # Remove nil values from e_vars before passing to Ansible
      e_vars.delete_if{|key, value| value.nil?}
   
      provision.vm.provision "ansible_local" do |ansible|
        # There's a bug in later versions of the Ansible docker-py module
        ansible.install_mode = "pip"
        ansible.version = "2.4.4.0"
        ansible.galaxy_command = "ansible-galaxy install --role-file=%{role_file} --roles-path=%{roles_path} --force"
        ansible.galaxy_roles_path = "/vagrant/roles/"
        ansible.galaxy_role_file = "requirements.yml"
        ansible.playbook = "playbooks/main.yml"
        ansible.extra_vars = e_vars
      end
    end 
  end 
end 
