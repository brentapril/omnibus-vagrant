# Omnibus-Vagrant

Support toolkit to help manage variable GitLab inventory, reproduce issues across a wide variety of environments and perform log analysis.

# Quick Start

- Install software listed in the [Requirements](https://gitlab.com/atanayno/omnibus-vagrant/edit/master/README.md#requirements) section
- Clone this repo to your local workstation and switch to the cloned folder
- In this folder create a file `gitlab-vagrant.yml` with the required environments listed, see [Example](https://gitlab.com/gl-support/omnibus-vagrant/tree/master#example-gitlab-vagrantyml-inventory) for details
- Run `vagrant up name` where `name` corresponds to one instance names from `gitlab-vagrant.yml`

## Requirements:
[Virtualbox 5.1+](https://www.virtualbox.org/wiki/Downloads)

[Vagrant 1.9+](https://www.vagrantup.com/downloads.html)

##### Optional (for Mac only):
[VMware Fusion](https://www.vmware.com/products/fusion/fusion-evaluation.html)

# Managing your virtual machines

_Note: Playbooks can be executed directly against non-virtual instances, see [playbooks](playbooks/README.md) for further information._

All instances are managed from a single inventory file _gitlab-vagrant.yml_ which resides in the root of the repository.

#### Usage

`vagrant {up|provision|ssh|halt|status|reload|destroy} <name_of_instance>`

## Omnibus Parameters

Deploy specific versions of Omnibus GitLab into a virtual environment.

Add parameters to file _gitlab-vagrant.yml_ in root of repository.

| Parameters   | Required             | Format       | Allowed Values       | Description |
| :----------- | :------------------- | :----------- | :------------------- | :---------- |
| name         | Yes                  | alphanumeric | Any                  |             |
| ip_address   | Yes                  | ipv4         | ipv4                 |             |
| memory       | No (default 4096)    | numeric      | nnnn                 |             |
| version      | Yes                  | x.x.x        | n.n+.n               |             |
| edition      | Yes                  | alphanumeric | ce or ee             |             |

#### Example gitlab-vagrant.yml inventory

```yaml
---
- name: gitlab-ee-9-3-9
  version: 9.3.9
  edition: ee
  ip_address: 192.168.15.53

- name: gitlab-with-es
  ip_address: 192.168.15.75
  memory: 4272
  version: 9.3.9
  edition: ee
  elasticsearch: 5.3.1
```

Using the above example you can now create a new instance with `vagrant up gitlab-ee-9-3-9` or `vagrant up gitlab-with-es`.

#### Provision from .deb package (optional)

Ansible will look for the .deb in `omnibus_packages/` before attempting any downloads from the repository, so if necessary drop your `.deb` in here to speed up the provisioning process.

#### Connect to Omnibus application

Application will be accessible on `http://<ip_address>` of your host machine.

External_url will be set to the _name_ parameter.

To resolve by hostname update `/etc/hosts` on host machine:
`sudo echo "<ip address> <name>" >> /etc/hosts`

## Additional services

Docker services can be deployed a standalone instance or alongside your Omnibus instances.

Deployed as docker containers onto the target instance.  This allows for ease of testing of GitLabs integrated apps.

_Note: Adding additional services is trivial, please visit [playbooks](playbooks/README.md#building-your-own-module-customization) for further information._

| Parameters   | Required             | Format       | Allowed Values       | URL |
| :----------- | :------------------- | :----------- | :------------------- | :---|
| name         | When standalone      | alphanumeric | Any                  |     |
| ip_address   | When standalone      | ipv4         | ipv4                 |     |
| memory       | No (default 4096)    | numeric      | nnnn                 |     |
| squid_proxy  | No                   | alphanumeric | docker tag           | https://hub.docker.com/r/sameersbn/squid/tags/ |
| gitlab-runner| No                   | alphanumeric | docker tag           | https://hub.docker.com/r/gitlab/gitlab-runner/tags/ |
| elasticsearch| No                   | alphanumeric | docker tag           | https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html |

Example _gitlab-vagrant.yml_:

```yaml
- name: gitlab-runner
  ip_address: 192.168.15.74
  memory: 1024
  gitlab_runner: v9.4.1
```

## Support Tools

Support tools should be deployed to a standalone instance.

| Parameters   | Required             | Format       | Allowed Values       | URL |
| :----------- | :------------------- | :----------- | :------------------- | :---|
| name         | Yes                  | alphanumeric | Any                  |     |
| ip_address   | Yes                  | ipv4         | ipv4                 |     |
| memory       | No (default 4096)    | numeric      | nnnn                 |     |
| elk          | No                   | boolean      | yes / no             |     |

#### ELK - Elasticsearch + Logstash + Kibana stack

_Note:  This is intended for local virtual machine only.  For security reasons, we must not upload customer logs to cloud providers such as Digital Ocean._

Example _gitlab-vagrant.yml_:

```yaml
- name: log-analysis
  memory: 4272
  ip_address: 192.168.15.53
  elk: yes
```

Once provisioned you can feed `production_json.log` or any other json formatted log files via TCP to the ELK stack from your workstation using Netcat:

```
nc <elk_ip_address> 5000 < production_json.log
```

Then open the IP address in the browser and you will be redirected to Kibana.  

##### Using Kibana
1. Click "Discover" and create your index `logstash-$(date)`, click "Next"
2. Select "time" field for sorting, click "Create index pattern"
3. Go to "Discover" again and ensure that your Kibana date range (top right hand corner) overlaps with the datestamps of the logs.  
4. Use the fields on the left hand side to filter events

Please see this short video tutorial [here](https://www.youtube.com/embed/BacBQiTonzU) to help familiarize yourself with Kibana.